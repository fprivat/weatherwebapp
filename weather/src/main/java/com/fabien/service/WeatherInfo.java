package com.fabien.service;

public class WeatherInfo {
    String city = "";
	String overall_weather = "";
    String temperature_F = "F";
    String temperature_C = "C";
	String sunrise_time = "";
	String sunset_time = "";
	String weather_date = "";
	
	
    public WeatherInfo() {
	}
    
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public String getOverall_weather() {
		return overall_weather;
	}
	public void setOverall_weather(String overall_weather) {
		this.overall_weather = overall_weather;
	}
	public String getTemperature_F() {
		return temperature_F;
	}
	public void setTemperature_F(String temperature_F) {
		this.temperature_F = temperature_F;
	}
	public String getTemperature_C() {
		return temperature_C;
	}
	public void setTemperature_C(String temperature_C) {
		this.temperature_C = temperature_C;
	}
	public String getSunrise_time() {
		return sunrise_time;
	}
	public void setSunrise_time(String sunrise_time) {
		this.sunrise_time = sunrise_time;
	}
	public String getSunset_time() {
		return sunset_time;
	}
	public void setSunset_time(String sunset_time) {
		this.sunset_time = sunset_time;
	}
	public String getWeather_date() {
		return weather_date;
	}
	public void setWeather_date(String weather_date) {
		this.weather_date = weather_date;
	}

}
