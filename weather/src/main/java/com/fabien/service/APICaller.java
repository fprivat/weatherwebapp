package com.fabien.service;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.StringReader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;

import org.w3c.dom.*;
import javax.xml.xpath.*;
import javax.xml.parsers.*;

import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

/**
 * Caller of the API weather service
 */
public class APICaller {
	// TODO move to configuration file
    private static final String apiKey="428f7185e15e59add937b95609c5a803";
    private static final String baseAPIUrl=
        "http://api.openweathermap.org/data/2.5/weather?appid="+apiKey+"&mode=xml&units=imperial&q=";
    
    private static WeatherInfo convertXMLtoWeatherInfo(String xml) throws ParserConfigurationException, SAXException, 
    IOException, XPathExpressionException
    { 
    	WeatherInfo weatherInfo = new WeatherInfo();
    	  
    	// Convert xml String to Document
		DocumentBuilderFactory domFactory = DocumentBuilderFactory.newInstance();
		domFactory.setNamespaceAware(true); 
		DocumentBuilder builder = domFactory.newDocumentBuilder();
		InputSource is = new InputSource(new StringReader(xml));
		Document doc = builder.parse(is);
		  
		// XPath query to select relevant information from the xml nodes
		XPath xpath = XPathFactory.newInstance().newXPath();
		
		XPathExpression expr = xpath.compile("//weather/@value");
		weatherInfo.setOverall_weather( (String) expr.evaluate(doc, XPathConstants.STRING) );

		expr = xpath.compile("//city/@name");
		weatherInfo.setCity( (String) expr.evaluate(doc, XPathConstants.STRING) );

		expr = xpath.compile("//temperature/@value");
		Double temperatureF = (Double) expr.evaluate(doc, XPathConstants.NUMBER);
		String formattedTempF = String.format("%.2f", temperatureF.doubleValue());
		weatherInfo.setTemperature_F( formattedTempF );
		
		double temperatureC = (double) (((temperatureF.floatValue() - 32.0)*5.0)/9.0);
		String formattedTempC = String.format("%.2f", temperatureC);
		weatherInfo.setTemperature_C( formattedTempC );
		
		// TODO convert GMT time to City time
		expr = xpath.compile("//city/sun/@rise");
		weatherInfo.setSunrise_time( (String) expr.evaluate(doc, XPathConstants.STRING) );
		
		expr = xpath.compile("//city/sun/@set");
		weatherInfo.setSunset_time( (String) expr.evaluate(doc, XPathConstants.STRING) );

		expr = xpath.compile("//lastupdate/@value");
		weatherInfo.setWeather_date( (String) expr.evaluate(doc, XPathConstants.STRING) );

		return weatherInfo;
    }
    
    public static WeatherInfo getWeatherInfo(String cityName)
    {
        String xmlResult=getWeatherAPIXMLString(cityName);
        
        try {
        	return(convertXMLtoWeatherInfo(xmlResult));
        }
        catch(Exception e) {
        	return(new WeatherInfo());
        }
    }
    
    //Get the weather of the specific city
    private static String getWeatherAPIXMLString(String cityName) throws RuntimeException{  
        
        //define a variable to store the weather api url and set beijing as it's default value
        String apiUrl = baseAPIUrl+"London";
  
        try {
            if(cityName!=null && cityName!="")
                apiUrl = baseAPIUrl+URLEncoder.encode(cityName, "utf-8");                    
        } catch (UnsupportedEncodingException e1) {               
            e1.printStackTrace();                     
        }  

        StringBuilder strBuf = new StringBuilder();          
        HttpURLConnection conn=null;
        BufferedReader reader=null;
        
        try{  
            //Declare the connection to weather api url
            URL url = new URL(apiUrl);  
            conn = (HttpURLConnection)url.openConnection();  
            conn.setRequestMethod("GET");
            
            if (conn.getResponseCode() != 200) {
                throw new RuntimeException("HTTP GET Request Failed with Error code : "
                              + conn.getResponseCode());
            }
            
            // Read the content from the weather api connection
	        reader = new BufferedReader(new InputStreamReader(conn.getInputStream(),"utf-8"));
            String output = null;  
            while ((output = reader.readLine()) != null)  
                strBuf.append(output);  
        }catch(MalformedURLException e) {  
            e.printStackTrace();   
        }catch(IOException e){  
            e.printStackTrace();   
        }
        finally
        {
            if(reader!=null)
            {
                try {
                    reader.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            if(conn!=null)
            {
                conn.disconnect();
            }
        }

        return strBuf.toString();  
    }
    
    
    
}