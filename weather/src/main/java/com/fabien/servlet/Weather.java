package com.fabien.servlet;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.fabien.service.APICaller;
import com.fabien.service.WeatherInfo;


/**
 * Servlet implementation class Weather
 */
public class Weather extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Weather() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// Get and validate name.
        String city = request.getParameter("city");
        String overall_weather = "";
        String temperature_F = "F";
        String temperature_C = "C";
    	String sunrise_time = "";
    	String sunset_time = "";
    	String weather_date = "";

		// Call the weather API to get weather info for that City
		WeatherInfo weatherInfo = APICaller.getWeatherInfo(city);
		
        request.setAttribute("city", weatherInfo.getCity());
        request.setAttribute("overall_weather", weatherInfo.getOverall_weather());
        request.setAttribute("temperature_F", weatherInfo.getTemperature_F());
        request.setAttribute("temperature_C", weatherInfo.getTemperature_C());
        request.setAttribute("sunrise_time", weatherInfo.getSunrise_time());
        request.setAttribute("sunset_time", weatherInfo.getSunset_time());
        request.setAttribute("weather_date", weatherInfo.getWeather_date());
        
        request.getRequestDispatcher("/weather.jsp").forward(request, response);
	}

}
