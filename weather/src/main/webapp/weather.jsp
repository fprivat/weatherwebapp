<html>
    <head>
        <title>Your weather forecast</title>
    </head>
    <body>
        <h1>From OpenWeatherMap.org</h1>
        <table>
            <tr>
                <th>City name</th>
                <th>Overall weather</th>
                <th>Temperature</th>
                <th>Sunrise time</th>
                <th>Sunset time</th>
                <th>Date</th>
            </tr>
            <tr>
                    <td><%= request.getAttribute("city") %></td>
                    <td><%= request.getAttribute("overall_weather") %></td>
                    <td><%= request.getAttribute("temperature_F") %> (F) / <%= request.getAttribute("temperature_C") %> (C)</td>
                    <td><%= request.getAttribute("sunrise_time") %></td>
                    <td><%= request.getAttribute("sunset_time") %></td>
                    <td><%= request.getAttribute("weather_date") %></td>
            </tr>
       </table>
    </body>
</html>