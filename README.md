** This web application queries OpenWeatherMap.org to retrieve weather data for London or Hong Kong.

This dynamic web application can be compile with Maven 3.3.9 and Java 8

The code has been tested using a Maven build under Eclipse and Jetty as a local Server.

The resulting website is then accessible at http://127.0.0.1:8080/weather/index.jsp or http://127.0.0.1:8080/weather

---
## Pre requisites and installation

	* Install Java SE8
	* Install Git
	* Install Maven 3.3.9
	* Set up PATH variables if you run the above tools from the command line
	
	* Clone this repository to your local folder: git clone https://fprivat@bitbucket.org/fprivat/weatherwebapp.git
	* Run Maven on this project (pom.xml provided) with the following Goals: mvn clean compile test install
	* If you're testing with Jetty
		* copy the WAR file generated by the Maven command (weather.war) into Jetty webapps subdirecty $JETTY_HOME/webapps
		* from $JETTY_HOME run: java -jar start.jar
	* Open your internet browser and access: http://127.0.0.1:8080/weather

## TODO
	 Externalize hardcoded values such as API Url, API Key
	 Create Unit testing classes
	 Add input validation
	 Add error handling (exceptions and HTML error codes)
	 Convert date time to the City local date time
	 Trim sun rise/set datetime into time only
	
## Requirements

	 Create a website which will return current weather data from OpenWeatherMap.org, based on a city chosen by the user, which should support:
		o London
		o Hong Kong

	 the user should be able to input their choice of city via a standard HTML <form> & receive results showing:
		o today’s date
		o the city name
		o overall description of the weather (e.g. "Light rain", "Clear sky", etc.)
		o temperature in Fahrenheit and Celsius
		o sunrise and sunset times in 12 hour format (e.g. 9:35am; 11:47pm)

	 the styling / layout of the website itself is not a priority — please feel free to use plain HTML with no styling

## Implementation details

	 the backend of the application should use core Java SE, EE and any 3rd party libraries that you deem relevant
	 the project should be able to be compiled on a machine with Maven 3.3 and Java SE8.
	Please give full instructions for set-up, as needed
	 the website should able to be run on a port of ‘localhost’, via some embedded application server (e.g. Jetty or similar)
	
---

